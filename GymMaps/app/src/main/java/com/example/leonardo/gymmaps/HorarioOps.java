package com.example.leonardo.gymmaps;

import android.app.Activity;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import database.FirebaseMediator;
import models.Gym;
import models.Horario;
import utils.ConfigurableOps;
import utils.GenericValueListener;
import utils.GenericValueListenerOps;

/**
 * Created by leonardo on 21/08/16.
 */
public class HorarioOps implements ConfigurableOps, GenericValueListenerOps {
    public final String TAG = this.getClass().getSimpleName();
    private WeakReference<HorarioActivity> activity;
    private Horario horario=null;
    private FirebaseMediator databaseMediator;

    @Override
    public void onConfiguration(Activity activity, boolean firstTimeIn) {
        Log.d(TAG, "onConfiguration: entering");
        this.activity = new WeakReference<>((HorarioActivity) activity);
        if (databaseMediator==null)
            databaseMediator = new FirebaseMediator();
    }

    @SuppressWarnings("unchecked")
    public void getHorario(String key, String data){
        databaseMediator.receiveHorario(key,data,new GenericValueListener(this));
    }


    @Override
    public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
        if (activity.get()==null) return;
        horario = new Horario();
        horario= dataSnapshot.getValue(Horario.class);
        activity.get().fill(horario);

    }

    @Override
    public void onChildAdded(com.google.firebase.database.DataSnapshot snapshot, String previousChildKey) {

    }
}
