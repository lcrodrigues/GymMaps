package com.example.leonardo.gymmaps;

import android.app.DialogFragment;
import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import database.FirebaseMediator;
import models.Gym;
import models.Horario;
import utils.GenericActivity;

public class GymActivity extends GenericActivity<GymOps> {

    private Gym mGym;
    public static String mKey;
    private FloatingActionButton addHorario;
    public static String NOME = "place_name";
    public static String EXTRA = "com.example.leonardo.gymmaps.GymActivity";
    public CollapsingToolbarLayout collapsingToolbarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, GymOps.class);
        setContentView(R.layout.activity_gym);

        Intent intent = getIntent();
        final String gymKey = intent.getStringExtra(MapsActivity.EXTRA);

        ((GymOps) getOps()).getGym(gymKey);

        addHorario = (FloatingActionButton) findViewById(R.id.fabAddH);

        addHorario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), HorarioActivity.class);
                intent.putExtra(EXTRA, gymKey);
                intent.putExtra(NOME, mGym.getName());
                startActivity(intent);
            }
        });



    }

    public void fillFields(Gym gym, String key) {
        if (gym != null)
            mGym = gym;

        mKey = key;

        TextView gName = (TextView) findViewById(R.id.nameText);
        gName.setText(gym.getName());

        TextView gAddress = (TextView) findViewById(R.id.addressText);
        gAddress.setText(gym.getAdress());

        TextView gPhone = (TextView) findViewById(R.id.phoneText);
        gPhone.setText(gym.getPhone());


        TextView gSport = (TextView) findViewById(R.id.sportText);
        gSport.setText(gym.getSports());

    }
}
