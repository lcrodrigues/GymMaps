package com.example.leonardo.gymmaps;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import database.FirebaseMediator;
import models.Gym;
import utils.GenericActivity;

import static com.google.android.gms.common.GooglePlayServicesUtil.*;

public class MapsActivity extends GenericActivity<Mapsops> implements OnMapReadyCallback{

    private GoogleMap mMap;
    private List<String> mKeys;
    private BottomSheetBehavior bottomSheetBehavior;
    private View bottomSheet;
    private List<Gym> mGyms;

    public final static String EXTRA = "com.example.leonardo.gymmaps";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, Mapsops.class);
        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Firebase.setAndroidContext(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), Formulario.class);
                startActivity(intent);
            }
        });
        ((Mapsops) getOps()).getGym();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main2, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_open:
                Intent intent = new Intent(getApplicationContext(), MeusHorariosActivity.class);
                startActivity(intent);
                break;
        }
        return true;
    }
    public void openMeusHorarios(){

    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        LatLng myLocation = new LatLng(-31.764834, -52.342034);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Intent intent = new Intent(getApplicationContext(), GymActivity.class);
                intent.putExtra(EXTRA, marker.getTitle());
                startActivity(intent);
                return true;
            }
        });

    }

    public void fillList(List<Gym> gyms, List<String> gyms_keys){
        if (gyms!=null)
            mGyms=gyms;
        mKeys=gyms_keys;
        for(int i=0; i<gyms.size();i++){
            Log.e("Gyms",mGyms.get(i).getAdress());
            //Geocoder geocoder = new Geocoder(this);
            //try {
            //List<Address> adresses;
            //adresses = geocoder.getFromLocationName(mGyms.get(i).getAdress(), 1);
            LatLng latLng = new LatLng(Double.parseDouble(mGyms.get(i).getLat()), Double.parseDouble(mGyms.get(i).getLongi()));
            mMap.addMarker(new MarkerOptions().position(latLng).title(mKeys.get(i)));
            //} catch (IOException e) {
            //  e.printStackTrace();
            //}
        }

    }

}