package com.example.leonardo.gymmaps;

import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import java.util.Calendar;
import java.util.List;

import models.MeusHorarios;
import utils.GenericActivity;

public class MeusHorariosActivity extends GenericActivity<MeusHorariosOps> {
    public static final String ID_SUBTOPIC = "id_subtopic";
    public static final String ID_SENDER = "id_sender";
    public static final String CONTENT = "content";
    public static final String TITLE = "title";
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private View bottomSheet;
    private MeusHorariosActivity.OnListFragmentInteractionListener mListener;
    private BottomSheetBehavior bottomSheetBehavior;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, MeusHorariosOps.class);
        setContentView(R.layout.activity_meus_horarios);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        ((MeusHorariosOps) getOps()).getAnswers();
    }
    public void fillList(List<MeusHorarios> horarios){
        if (horarios!=null)
            mRecyclerView.setAdapter(new MeusHorariosAdapter(horarios, mListener));
    }
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(String item);
    }
}
