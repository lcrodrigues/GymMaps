package com.example.leonardo.gymmaps;

import android.app.Activity;
import android.util.Log;

import java.lang.ref.WeakReference;

import database.FirebaseMediator;
import models.Gym;
import utils.ConfigurableOps;
import utils.GenericValueListener;
import utils.GenericValueListenerOps;

/**
 * Created by leonardo on 20/08/16.
 */
public class GymOps implements ConfigurableOps, GenericValueListenerOps {
    public final String TAG = this.getClass().getSimpleName();
    private WeakReference<GymActivity> activity;
    private Gym gym = null;
    private String gKey = null;
    private FirebaseMediator databaseMediator;


    @Override
    public void onConfiguration(Activity activity, boolean firstTimeIn) {
        Log.d(TAG, "onConfiguration: entering");
        this.activity = new WeakReference<>((GymActivity) activity);
        if (databaseMediator==null)
            databaseMediator = new FirebaseMediator();
    }

    @SuppressWarnings("unchecked")
    public void getGym(String key){
        databaseMediator.receiveGym(new GenericValueListener(this), key);
    }


    @Override
    public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
        if (activity.get()==null) return;
        gym = new Gym();

        gym = dataSnapshot.getValue(Gym.class);
        activity.get().fillFields(gym, gKey);

    }

    @Override
    public void onChildAdded(com.google.firebase.database.DataSnapshot snapshot, String previousChildKey) {

    }
}