package utils;

import android.app.Activity;

public interface ConfigurableOps {
    void onConfiguration(Activity activity, boolean firstTimeIn);
}
