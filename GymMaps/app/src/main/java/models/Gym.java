package models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vplentz on 20/08/16.
 */

public class Gym {
    private String gym_name;
    private String phone;
    private String adress;
    private String owner;
    private String sports;
    private String lat;
    private String longi;

    public Gym(String gym_name, String phone, String adress, String owner, String sports, String lat, String longi) {
        this.gym_name = gym_name;
        this.phone = phone;
        this.adress = adress;
        this.owner = owner;
        this.sports = sports;
        this.lat = lat;
        this.longi = longi;
    }


    public String getLat() {
        return lat;
    }


    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLongi() {
        return longi;
    }

    public void setLongi(String longi) {
        this.longi = longi;
    }

    public Gym(){}

    public String getName() {
        return gym_name;
    }

    public void setGym_name(String gym_name) {
        this.gym_name = gym_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getSports() {
        return sports;
    }

    public void setSports(String sports) {
        this.sports = sports;
    }
}
