package com.example.leonardo.gymmaps;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import database.FirebaseMediator;
import models.User;
import utils.GenericValueListener;
import utils.GenericValueListenerOps;

/**
 * A simple {@link Fragment} subclass.
 */
public class FacebookFragment extends Fragment {
    private  String TAG = this.getClass().getSimpleName();

    public FacebookFragment() {
        // Required empty public constructor
    }

    private ProfileTracker profileTracker;
    private AccessTokenTracker tokenTracker;
    private LoginButton loginButton;

    private CallbackManager mCallbackManager;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private Profile myProfile;
    private FirebaseMediator mediator;

    @Override
    @SuppressWarnings("unchecked")
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCallbackManager = CallbackManager.Factory.create();
        mAuth = FirebaseAuth.getInstance();
        profileTracker= new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                if (currentProfile!=null){
                    Log.d(TAG, "onCurrentProfileChanged: ");
                    myProfile = currentProfile;
                }
            }
        };
        tokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if (currentAccessToken!=null)
                    Log.d(TAG, "onCurrentAccessTokenChanged: "+currentAccessToken.toString());
            }
        };

        mediator = new FirebaseMediator();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    String userId =  user.getUid();
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + userId);
                    mediator.receiveUser(userId,new GenericValueListener(new UserValueListenerOps()));
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                    loginButton.setVisibility(View.VISIBLE);
                }

            }
        };
        profileTracker.startTracking();
        tokenTracker.startTracking();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        loginButton = (LoginButton) rootView.findViewById(R.id.button_connect);
        loginButton.setReadPermissions("user_photos");
        loginButton.setFragment(this);
        loginButton.registerCallback(mCallbackManager, mCallback);
        return rootView;
    }

    private class UserValueListenerOps implements GenericValueListenerOps {

        @Override
        public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
            Log.d(TAG, "onDataChange: "+dataSnapshot.toString());
            User me = dataSnapshot.getValue(User.class);
            if (me==null){
                //cria novo estudante
                mediator.saveUser(new User(mAuth.getCurrentUser().getDisplayName()),
                        mAuth.getCurrentUser().getUid().toString());
                if (myProfile!=null) {

                }
                else{
                    loginButton.setVisibility(View.VISIBLE);
                }
            }
            else{
                Intent intent;
                // TODO: 13/08/16 pular setup se estudante já tiver curso
                    intent = new Intent(getActivity(),MapsActivity.class);

                startActivity(intent);
            }

        }

        @Override
        public void onChildAdded(com.google.firebase.database.DataSnapshot snapshot, String previousChildKey) {

        }
    }


    private FacebookCallback<LoginResult> mCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            Log.d(TAG, "onSuccess: "+loginResult.getAccessToken().toString());
            handleFacebookAccessToken(loginResult.getAccessToken());

            Intent intent = new Intent(getActivity(),MapsActivity.class);
            startActivity(intent);

        }

        @Override
        public void onCancel() {
            //TODO, implement onCancel
        }

        @Override
        public void onError(FacebookException e) {
            Log.e(TAG, "onError: "+e.toString() );
        }
    };

    private void handleFacebookAccessToken(AccessToken token) {
        if (token==null){
            Log.e(TAG, "handleFacebookAccessToken: NULL" );
            return;
        }


        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" );


                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(getActivity().getApplicationContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                })
                .addOnFailureListener(getActivity(), new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "onFailure: "+e.toString() );
                    }
                })
        ;


        /*String userId=mAuth.getCurrentUser().getUid();
        SharedPreferences.Editor editor=preferences.edit();
        editor.putString(USER_ID_TAG,userId);
        editor.apply();
        mediator.receiveStudent(userId,new GenericValueListener(new UserValueListenerOps()));*/
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: ");
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        profileTracker.startTracking();
        tokenTracker.startTracking();
    }

    @Override
    public void onPause() {
        super.onPause();
        profileTracker.stopTracking();
        tokenTracker.stopTracking();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        tokenTracker.stopTracking();
        profileTracker.stopTracking();
    }
}
