

package com.example.leonardo.gymmaps;

import android.app.Activity;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import database.FirebaseMediator;
import models.Gym;
import utils.ConfigurableOps;
import utils.GenericValueListener;
import utils.GenericValueListenerOps;

/**
 * Created by vplentz on 20/08/16.
 */

public class Mapsops implements ConfigurableOps, GenericValueListenerOps {
    public final String TAG = this.getClass().getSimpleName();
    private WeakReference<MapsActivity> activity;
    private List<Gym> gyms=null;
    private List<String> gyms_keys=null;
    private FirebaseMediator databaseMediator;

    @Override
    public void onConfiguration(Activity activity, boolean firstTimeIn) {
        Log.d(TAG, "onConfiguration: entering");
        this.activity = new WeakReference<>((MapsActivity) activity);
        if (databaseMediator==null)
            databaseMediator = new FirebaseMediator();
    }

    @SuppressWarnings("unchecked")
    public void getGym(){
        databaseMediator.receiveGyms(new GenericValueListener(this));
    }


    @Override
    public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
        if (activity.get()==null) return;
        gyms = new ArrayList<>();
        gyms_keys = new ArrayList<>();
        for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
            gyms.add(postSnapshot.getValue(Gym.class));
            gyms_keys.add(postSnapshot.getKey());
        }
        Collections.reverse(gyms);
        activity.get().fillList(gyms, gyms_keys);

    }

    @Override
    public void onChildAdded(com.google.firebase.database.DataSnapshot snapshot, String previousChildKey) {

    }
}

