package models;

/**
 * Created by vplentz on 21/08/16.
 */

public class MeusHorarios{
    private String gymName;
    private String dia;
    private String mes;
    private  String ano;
    private String hora;

    public MeusHorarios(String gymName, String dia, String mes, String ano,String hora) {
        this.gymName = gymName;
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
        this.hora = hora;
    }

    public MeusHorarios(){}

    public String getGymName() {
        return gymName;
    }
    public String getAno(){return ano;}
    public String getDia() {
        return dia;
    }

    public String getMes() {
        return mes;
    }

    public String getHora() {
        return hora;
    }
}
