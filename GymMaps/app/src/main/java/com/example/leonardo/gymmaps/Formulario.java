package com.example.leonardo.gymmaps;

import android.content.Intent;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import database.FirebaseMediator;
import models.Gym;

public class Formulario extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

    }

    public void sendNewGym(View v){
        EditText edtNomeGin = (EditText) findViewById(R.id.gymName);
        EditText edtTel = (EditText) findViewById(R.id.gymPhone);
        EditText edtEsporte = (EditText) findViewById(R.id.gymSport);
        EditText edtEndereco = (EditText) findViewById(R.id.gymAddress);
        EditText edtLat = (EditText) findViewById(R.id.gymLat);
        EditText edtLong = (EditText) findViewById(R.id.gymLong);
        FirebaseMediator firebaseMediator = new FirebaseMediator();
        firebaseMediator.saveGym(new Gym(edtNomeGin.getText().toString(), edtTel.getText().toString(),
                edtEndereco.getText().toString(), null,edtEsporte.getText().toString(), edtLat.getText().toString()
                , edtLong.getText().toString()));
        edtEndereco.setText(null);
        edtTel.setText(null);
        edtEsporte.setText(null);
        edtNomeGin.setText(null);

        Intent intent = new Intent(v.getContext(), MapsActivity.class);
        startActivity(intent);
    }
}
