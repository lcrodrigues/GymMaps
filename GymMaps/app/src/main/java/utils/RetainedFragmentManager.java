package utils;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.HashMap;

public class RetainedFragmentManager {
    public final String TAG = this.getClass().getSimpleName();

    private final String retainedFragmentTag;
    private final WeakReference<FragmentManager> manager;
    private RetainedFragment retainedFragment;

    public RetainedFragmentManager(FragmentManager manager, String retainedFragmentTag){
        this.manager = new WeakReference<>(manager);
        this.retainedFragmentTag=retainedFragmentTag;
    }

    public boolean firstTimeIn(){
        try {
           retainedFragment=(RetainedFragment) manager.get().findFragmentByTag(retainedFragmentTag);
            if (null==retainedFragment){
                retainedFragment = new RetainedFragment();
                manager.get().beginTransaction().add(retainedFragment,retainedFragmentTag).commit();
                return true;
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        return false;
    }

    public void put(Object object){
        retainedFragment.put(object);
    }


    @SuppressWarnings("unchecked")
    public <T> T get(String key){
        return (T) retainedFragment.get(key);
    }

    public static class RetainedFragment extends Fragment{
        private HashMap<String, Object> data=new HashMap<>();

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
        }

        public void put(String key, Object object){
            data.put(key, object);
        }

        public void put(Object object){
            Log.d("frag", "put: putting "+object.getClass().getName());
            put(object.getClass().getName(),object);
        }

        @SuppressWarnings("unchecked")
        public <T> T get (String key){
            Log.d("frag", "get: getting "+key);
            return (T) data.get(key);
        }
    }

}
