package models;

/**
 * Created by vplentz on 20/08/16.
 */

public class User {
    private String name;

    public User(String name) {
        this.name = name;
    }
    public User(){}
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
