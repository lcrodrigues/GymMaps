package database;


import android.util.Log;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.database.DatabaseError;

import java.util.ArrayList;
import java.util.List;

import models.Gym;
import models.Horario;
import models.MeusHorarios;
import models.User;
import utils.GenericValueListener;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;


/**
 * Created by vplentz on 20/08/16.
 */
public class FirebaseMediator{

    String firebaseRef="https://gymmaps-1c504.firebaseio.com/";
    DatabaseReference firebase;
    public void receiveGyms(GenericValueListener listener){
        firebase = FirebaseDatabase.getInstance().getReferenceFromUrl(firebaseRef);
        firebase.child("gym").addValueEventListener(listener);
    }

    public void receiveGym(GenericValueListener listener, String key){
        firebase = FirebaseDatabase.getInstance().getReferenceFromUrl(firebaseRef);
        firebase.child("gym").child(key).addValueEventListener(listener);
    }

    public void saveGym(Gym gym){
        firebase = FirebaseDatabase.getInstance().getReferenceFromUrl(firebaseRef).child("gym");
        firebase.push().setValue(gym);
    }
    public void saveMeusHorarios(String mykey, MeusHorarios meusHorarios){
        firebase = FirebaseDatabase.getInstance().getReferenceFromUrl(firebaseRef).child("meusHorarios")
                .child(mykey);
        firebase.push().setValue(meusHorarios);
    }
    public void receiveMeusHorarios(String myKey, GenericValueListener listener){
        firebase = FirebaseDatabase.getInstance().getReferenceFromUrl(firebaseRef);
        firebase.child("meusHorarios").child(myKey).addValueEventListener(listener);
    }
    public void saveHorario(String keyGym, Horario horario){
        firebase = FirebaseDatabase.getInstance().getReferenceFromUrl(firebaseRef).child("horario")
                .child(keyGym);
        firebase.push().setValue(horario);
    }
    public  void receiveHorario(String keyGym, String data,GenericValueListener listener){
        firebase = FirebaseDatabase.getInstance().getReferenceFromUrl(firebaseRef);
        Query queryRef = firebase.child("horario").child(keyGym).orderByChild("data").equalTo(data);
        queryRef.addListenerForSingleValueEvent(listener);

    }
    public void saveUser(User user, String userId){
        firebase = FirebaseDatabase.getInstance().getReferenceFromUrl(firebaseRef).child("user").child(userId);
        firebase.push().setValue(user);
    }

    public void receiveUser(String userId, GenericValueListener listener) {
        firebase = FirebaseDatabase.getInstance().getReferenceFromUrl(firebaseRef);
        firebase.child("user").child(userId)
                .addValueEventListener(listener);
    }
}
