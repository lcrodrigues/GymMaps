package models;

/**
 * Created by leonardo on 21/08/16.
 */
public class Horario {


    private String data;
    private String nome;

    public Horario(){}

    public Horario(String data, String nome){
        this.setData(data);
        this.setNome(nome);
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
