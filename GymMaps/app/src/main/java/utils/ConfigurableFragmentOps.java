package utils;

import android.app.Activity;
import android.support.v4.app.Fragment;

import java.lang.ref.WeakReference;

public abstract class ConfigurableFragmentOps {
    protected WeakReference<Fragment> fragment;
    public abstract void onConfiguration(Activity activity, boolean firstTimeIn);
    public void setFragment(Fragment fragment){
        this.fragment = new WeakReference<>(fragment);
    }
}
