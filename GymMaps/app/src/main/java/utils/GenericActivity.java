package utils;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class GenericActivity<OpsType extends ConfigurableOps>
        extends AppCompatActivity {
    public final String TAG = this.getClass().getSimpleName();
    private final RetainedFragmentManager retainedFragmentManager
            =new RetainedFragmentManager(this.getFragmentManager(),TAG);

    private ConfigurableOps opsInstance;

    public ConfigurableOps getOps(){
        return opsInstance;
    }

    protected void onCreate(Bundle savedInstanceState,Class<OpsType> opsType) {
        super.onCreate(savedInstanceState);
        try {
            handleConfiguration(opsType);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleConfiguration(Class<OpsType> opsType) throws InstantiationException, IllegalAccessException {
        if (retainedFragmentManager.firstTimeIn())
            initialize(opsType);

        else {
            opsInstance=retainedFragmentManager.get(opsType.getName());
            if (opsInstance==null)
                initialize(opsType);
            else
                opsInstance.onConfiguration(this, false);

        }
    }

    public void initialize(Class<OpsType> opsType) throws IllegalAccessException, InstantiationException {
        opsInstance = opsType.newInstance();
        retainedFragmentManager.put(opsInstance);
        opsInstance.onConfiguration(this,true);
    }

    public RetainedFragmentManager getRetainedFragmentManager(){
        return retainedFragmentManager;
    }

}
