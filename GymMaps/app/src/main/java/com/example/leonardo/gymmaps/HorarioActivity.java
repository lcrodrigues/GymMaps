package com.example.leonardo.gymmaps;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;

import database.FirebaseMediator;
import models.Horario;
import models.MeusHorarios;
import utils.GenericActivity;

/**
 * Created by leonardo on 21/08/16.
 */
public class HorarioActivity extends GenericActivity<HorarioOps> {
    public static Integer hora, dia, mes, ano;
    public static Horario mhorario;
    public String data;
    public Button button;
    public Horario horario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, HorarioOps.class);
        setContentView(R.layout.activity_horario);

        Intent intent = getIntent();
        final String gymKey = intent.getStringExtra(GymActivity.EXTRA);
        DialogFragment t = new TimePickerFragment();
        t.show(getFragmentManager(), "timePicker");

        DialogFragment d = new DatePickerFragment();
        d.show(getFragmentManager(), "datePicker");

        button = (Button) findViewById(R.id.send);
        ((HorarioOps) getOps()).getHorario(gymKey, data);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data = hora.toString() + dia.toString() + mes.toString() + ano.toString();

               // if(mhorario==null){
                    FirebaseMediator firebaseMediator = new FirebaseMediator();
                    firebaseMediator.saveHorario(gymKey, new Horario(data, FirebaseAuth.getInstance().getCurrentUser().getDisplayName()));
                    firebaseMediator.saveMeusHorarios(FirebaseAuth.getInstance().getCurrentUser().getUid(),
                      new MeusHorarios(getIntent().getExtras().getString(GymActivity.NOME),dia.toString(), mes.toString(),
                              ano.toString(), hora.toString()));
                // /     }else
             //       Toast.makeText(HorarioActivity.this,"Horário ocupado",Toast.LENGTH_SHORT).show();

                Intent intent1 = new Intent(view.getContext(), MapsActivity.class);
                startActivity(intent1);

            }
        });

    }

    public void fill(Horario horario){
        if (horario!=null)
            mhorario=horario;
    }

}
