package utils;


import com.google.firebase.database.DataSnapshot;

public interface GenericValueListenerOps {
    void onDataChange(DataSnapshot dataSnapshot);
    void onChildAdded(DataSnapshot snapshot, String previousChildKey);
}
