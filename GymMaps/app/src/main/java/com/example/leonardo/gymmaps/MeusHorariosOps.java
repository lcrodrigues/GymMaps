package com.example.leonardo.gymmaps;

import android.app.Activity;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import database.FirebaseMediator;
import models.MeusHorarios;
import utils.ConfigurableOps;
import utils.GenericValueListener;
import utils.GenericValueListenerOps;

/**
 * Created by vplentz on 21/08/16.
 */

public class MeusHorariosOps implements ConfigurableOps, GenericValueListenerOps {
    public final String TAG = this.getClass().getSimpleName();
    private WeakReference<MeusHorariosActivity> activity;
    private List<MeusHorarios> horarios=null;
    private FirebaseMediator databaseMediator;

    @Override
    public void onConfiguration(Activity activity, boolean firstTimeIn) {
        Log.d(TAG, "onConfiguration: entering");
        this.activity = new WeakReference<>((MeusHorariosActivity) activity);
        if (databaseMediator==null)
            databaseMediator = new FirebaseMediator();
    }

    @SuppressWarnings("unchecked")
    public void getAnswers(){
        databaseMediator.receiveMeusHorarios(FirebaseAuth.getInstance().getCurrentUser().getUid(),new GenericValueListener(this));
    }


    @Override
    public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
        if (activity.get()==null) return;
        horarios = new ArrayList<>();
        for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
            horarios.add(postSnapshot.getValue(MeusHorarios.class));
        }
        Collections.reverse(horarios);
        activity.get().fillList(horarios);

    }

    @Override
    public void onChildAdded(com.google.firebase.database.DataSnapshot snapshot, String previousChildKey) {

    }
}
