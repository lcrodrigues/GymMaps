package utils;


import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

public class GenericValueListener <Ops extends GenericValueListenerOps> implements ValueEventListener, ChildEventListener {
    private Ops ops;

    public GenericValueListener(Ops ops){
        this.ops=ops;
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        ops.onDataChange(dataSnapshot);
    }


    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        ops.onChildAdded(dataSnapshot,s);
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }


    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

}
