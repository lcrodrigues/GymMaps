package com.example.leonardo.gymmaps;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import models.MeusHorarios;

/**
 * Created by vplentz on 21/08/16.
 */

public class MeusHorariosAdapter extends RecyclerView.Adapter<MeusHorariosAdapter.ViewHolder> {

private final List<MeusHorarios> mValues;
private final MeusHorariosActivity.OnListFragmentInteractionListener mListener;

public MeusHorariosAdapter(List<MeusHorarios> items,
        MeusHorariosActivity.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
        }

@Override
public MeusHorariosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.item_meus_horarios, parent, false);
        return new MeusHorariosAdapter.ViewHolder(view);
        }


@Override
public void onBindViewHolder(final MeusHorariosAdapter.ViewHolder holder, final int position) {
        holder.mEndereco.setText(mValues.get(position).getGymName());
        holder.mHora.setText(mValues.get(position).getDia()+"/"+mValues.get(position).getMes()+"/"
        +mValues.get(position).getAno()+" às "+mValues.get(position).getHora());
        }

@Override
public int getItemCount() {
        return mValues.size();
        }

public class ViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    //public final ImageView mSenderImagem;
    public final TextView mEndereco;
    public final TextView mHora;
    public ViewHolder(View view) {
        super(view);
        mView = view;
        // mSenderImagem = (ImageView) view.findViewById(R.id.imageViewSender);
        mEndereco = (TextView) view.findViewById(R.id.textViewAdress);
        mHora = (TextView) view.findViewById(R.id.textViewHora);
    }

    @Override
    public String toString() {
        return super.toString() + " '" + mEndereco.getText() + "'";
    }
}
}
